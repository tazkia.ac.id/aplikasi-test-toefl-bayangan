package id.ac.tazkia.toeflbayangan.constant;

public enum StatusAnswer {

    CORRECT, INCORRECT, WAITING

}
