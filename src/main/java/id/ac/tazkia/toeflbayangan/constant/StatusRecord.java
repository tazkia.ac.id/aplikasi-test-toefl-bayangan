package id.ac.tazkia.toeflbayangan.constant;

public enum StatusRecord {

    ACTIVE, DONE, DELETED, FINISH

}
