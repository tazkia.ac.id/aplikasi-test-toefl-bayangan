package id.ac.tazkia.toeflbayangan.constant;

public enum StatusType {
    STRUCTURE, READING
}
