package id.ac.tazkia.toeflbayangan.controller;

import id.ac.tazkia.toeflbayangan.dao.DeskripsiSoalDao;
import id.ac.tazkia.toeflbayangan.dao.SoalDao;
import id.ac.tazkia.toeflbayangan.dto.soal.RequestSoal;
import id.ac.tazkia.toeflbayangan.entity.DeskripsiSoal;
import id.ac.tazkia.toeflbayangan.entity.Soal;
import id.ac.tazkia.toeflbayangan.service.SoalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("bank-soal")
public class BankSoalController {
    @Autowired
    private SoalService soalService;

    @Autowired
    private DeskripsiSoalDao deskripsiSoalDao;

    @Autowired
    private SoalDao soalDao;

    @GetMapping("/list")
    public String bankSoal(Model model, Pageable pageable) {

        model.addAttribute("desc", soalDao.getDeskripsi());
        model.addAttribute("soal", soalDao.getAllSoals());
        model.addAttribute("setting", "active");
        model.addAttribute("bank_soal", "active");
        return "bank_soal/list";
    }


    @GetMapping("/detail")
    public String bankSoalDetail(Model model, @RequestParam(required = false, value = "soalId", name = "soalId") String soalId,
                                 @RequestParam(required = false, value = "descriptionId", name = "descriptionId") DeskripsiSoal deskripsiSoal) {
        if (StringUtils.hasText(soalId)) {
            model.addAttribute("soal", soalDao.findByIdAndDeletedIsNull(soalId));
        }

        if (deskripsiSoal != null) {
            model.addAttribute("deskripsiSoal", deskripsiSoal);
            System.out.println("Deskripsi Soal : " + deskripsiSoal.getId());
            model.addAttribute("listSoal", soalDao.findByDeskripsiSoalOrderByNomor(deskripsiSoal));
        }

        model.addAttribute("setting", "active");
        model.addAttribute("bank_soal", "active");
        return "bank_soal/detail";
    }



    @GetMapping("/form")
    public String bankSoalNew(Model model, @RequestParam(required = false, value = "id", name = "id") String id) {
        if (StringUtils.hasText(id)) {
            model.addAttribute("descriptionId", deskripsiSoalDao.findById(id).get());
        }
        model.addAttribute("setting", "active");
        model.addAttribute("bank_soal", "active");
        return "bank_soal/form";
    }

    @GetMapping("/edit")
    public String bankSoalEdit(Model model, @RequestParam(required = true, value = "id", name = "id") String id) {
        System.out.println("id : "+ id);
        Soal soal = soalDao.findByIdAndDeletedIsNull(id);
        RequestSoal requestSoal = new RequestSoal();
        if (soal != null && soal.getDeskripsiSoal() != null) {
            requestSoal.setDeskripsi(soal.getDeskripsiSoal().getDeskripsi());
        }

        if (soal.getJawabanA()){
            requestSoal.setJawabanA("A");
        }
        if (soal.getJawabanB()){
            requestSoal.setJawabanB("B");
        }
        if (soal.getJawabanC()){
            requestSoal.setJawabanC("C");
        }
        if (soal.getJawabanD()){
            requestSoal.setJawabanD("D");
        }

        BeanUtils.copyProperties(soal,requestSoal);
        model.addAttribute("soal", requestSoal);
        model.addAttribute("setting", "active");
        model.addAttribute("bank_soal", "active");
        return "bank_soal/edit";
    }

    @GetMapping("/add")
    public String bankSoalAdd(Model model, @RequestParam(required = false, value = "id", name = "id") String id) {
        if (StringUtils.hasText(id)) {
            model.addAttribute("descriptionId", deskripsiSoalDao.findById(id).get());
        }
        model.addAttribute("setting", "active");
        model.addAttribute("bank_soal", "active");
        return "bank_soal/form";
    }

    @PostMapping("/form")
    public String saveSoal(@ModelAttribute RequestSoal request, ModelMap modelMap) throws Exception {

        try {

            Soal soal = soalService.saveSoal(request);
            if (request.getDeskripsi().isEmpty()) {
                return "redirect:list";

            } else {
                return "redirect:detail?descriptionId=" + soal.getDeskripsiSoal().getId();
            }
        } catch (Error e) {

            log.error("ERROR SAVING", e);
            modelMap.addAttribute("errorMessage", e.getMessage());
            modelMap.addAttribute("request", request);
            return "form";
        }
    }

    @GetMapping("/delete")
    public String deleteSoal(@RequestParam(required = true) String id){

        Soal soal = soalDao.findByIdAndDeletedIsNull(id);
        if (soal == null){
            soal = soalDao.findByIdAndDeletedIsNotNull(id);
        }
        soalDao.delete(soal);



        return "redirect:list";
    }
}
