package id.ac.tazkia.toeflbayangan.controller;


import id.ac.tazkia.toeflbayangan.constant.StatusAnswer;
import id.ac.tazkia.toeflbayangan.constant.StatusRecord;
import id.ac.tazkia.toeflbayangan.dao.SoalDao;
import id.ac.tazkia.toeflbayangan.dao.SoalTestDao;
import id.ac.tazkia.toeflbayangan.dao.SubscribeDao;
import id.ac.tazkia.toeflbayangan.dao.config.UserDao;
import id.ac.tazkia.toeflbayangan.entity.Soal;
import id.ac.tazkia.toeflbayangan.entity.SoalTest;
import id.ac.tazkia.toeflbayangan.entity.Subscribe;
import id.ac.tazkia.toeflbayangan.entity.config.User;
import id.ac.tazkia.toeflbayangan.service.CurrentUserService;
import id.ac.tazkia.toeflbayangan.service.FinishService;
import id.ac.tazkia.toeflbayangan.service.PdfService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.context.Context;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Controller
@Slf4j
public class TestController {

    @Autowired
    private SoalDao soalDao;

    @Autowired
    private SoalTestDao soalTestDao;

    @Autowired
    private CurrentUserService currentUserService;
    @Autowired
    private UserDao userDao;

    @Autowired
    private FinishService finishService;

    @Autowired
    private PdfService pdfService;

    @Autowired
    private SubscribeDao subscribeDao;



    @GetMapping("/test")
    public String test(Model model,
                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);


        List<Soal> soalList = soalDao.cariSoalUjian();

        for(Soal soal:soalList){
            SoalTest soalTest = soalTestDao.findByStatusNotAndUserAndSoal(StatusRecord.DELETED, user, soal);

            if(soalTest == null){
                SoalTest soalTestNew = new SoalTest();
                soalTestNew.setSoal(soal);
                soalTestNew.setUser(user);
                soalTestNew.setStatus(StatusRecord.ACTIVE);
                soalTestNew.setStatusAnswer(StatusAnswer.WAITING);
                soalTestDao.save(soalTestNew);
            }
        }

        List<SoalTest> soalTestList = soalTestDao.findByStatusNotAndUserOrderBySoalJenisAscSoalDeskripsiSoalCreatedAscSoalNomorAsc(StatusRecord.DELETED, user);
        Integer nomor = 1;
        for(SoalTest soalTest : soalTestList){
            soalTest.setNomor(nomor);
            soalTestDao.save(soalTest);
            nomor = nomor + 1;
        }


        return "test/dashboard";
    }

    @GetMapping("/test/start")
    public String mulaiTest(Model model,
                            @RequestParam(required = false) Integer nomor,
                            Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        if(user.getStatus() == StatusRecord.FINISH){
            return "test/form_finish";
        }else {

            if (user.getWaktuMulai() == null) {
                user.setWaktuMulai(LocalDateTime.now());
                LocalDateTime waktuSelesai = LocalDateTime.now().plus(60, ChronoUnit.MINUTES);
                user.setWaktuSelesai(LocalDateTime.now().plusMinutes(60));
                userDao.save(user);
            }

            if (nomor == null) {
                model.addAttribute("soalTest", soalTestDao.findByStatusNotAndUserAndNomor(StatusRecord.DELETED, user, 1));
            } else {
                model.addAttribute("soalTest", soalTestDao.findByStatusNotAndUserAndNomor(StatusRecord.DELETED, user, nomor));
            }

//            LocalDateTime endTime = LocalDateTime.of(2023, 11, 22, 23, 0, 0);

            // Waktu sekarang
            LocalDateTime now = LocalDateTime.now();

            // Hitung selisih waktu antara sekarang dan batas akhir
            Duration duration = Duration.between(now, user.getWaktuSelesai());

            // Format durasi menjadi jam, menit, dan detik
            long hours = duration.toHours();
            long minutes = duration.toMinutesPart();
            long seconds = duration.toSecondsPart();

            // Masukkan nilai ke dalam model
            model.addAttribute("hours", hours);
            model.addAttribute("minutes", minutes);
            model.addAttribute("seconds", seconds);

            model.addAttribute("listSoal", soalTestDao.findByStatusNotAndUserOrderByNomor(StatusRecord.DELETED, user));
            model.addAttribute("user", user);
            Integer jumlah = soalTestDao.countByStatusNotAndUser(StatusRecord.DELETED,user);
            model.addAttribute("countSoal", jumlah);

            return "test/form";
        }
    }

    @PostMapping("/test/answer")
    public String testAnswer(@RequestParam (required = true) SoalTest soalTest,
                             @RequestParam(required = false) String jawaban,
                             @RequestParam(required = false) String finish,
                             RedirectAttributes redirectAttributes,Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        if(jawaban == null){
            redirectAttributes.addFlashAttribute("error", "Please Answer this question.!");
            return "redirect:../test/start?nomor=" + soalTest.getNomor();
        }

        if(jawaban.isEmpty()){
            redirectAttributes.addFlashAttribute("error", "Please Answer this question.!");
            return "redirect:../test/start?nomor=" + soalTest.getNomor();
        }

        SoalTest soalTest1 = soalTestDao.findById(soalTest.getId()).get();
        if(jawaban.equals("A")){
            soalTest1.setAnswera(Boolean.TRUE);
            soalTest1.setAnswerb(Boolean.FALSE);
            soalTest1.setAnswerc(Boolean.FALSE);
            soalTest1.setAnswerd(Boolean.FALSE);
            if(soalTest1.getSoal().getJawabanA() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }else if(jawaban.equals("B")){
            soalTest1.setAnswera(Boolean.FALSE);
            soalTest1.setAnswerb(Boolean.TRUE);
            soalTest1.setAnswerc(Boolean.FALSE);
            soalTest1.setAnswerd(Boolean.FALSE);
            if(soalTest1.getSoal().getJawabanB() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }else if(jawaban.equals("C")){
            soalTest1.setAnswera(Boolean.FALSE);
            soalTest1.setAnswerb(Boolean.FALSE);
            soalTest1.setAnswerc(Boolean.TRUE);
            soalTest1.setAnswerd(Boolean.FALSE);
            if(soalTest1.getSoal().getJawabanC() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }else if(jawaban.equals("D")){
            soalTest1.setAnswera(Boolean.FALSE);
            soalTest1.setAnswerb(Boolean.FALSE);
            soalTest1.setAnswerc(Boolean.FALSE);
            soalTest1.setAnswerd(Boolean.TRUE);
            if(soalTest1.getSoal().getJawabanD() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }
        soalTest1.setJawaban(jawaban);
        soalTest1.setStatus(StatusRecord.DONE);
        soalTest1.setTanggalUpdate(LocalDateTime.now());
        soalTest1.setUserUpdate(user.getUsername());
        soalTestDao.save(soalTest1);

        Integer jumlah = soalTestDao.countByStatusNotAndUser(StatusRecord.DELETED,user);
        System.out.println("jawaban = "+ jawaban);
        Integer nomor = soalTest.getNomor() + 1;
        if(nomor <= jumlah) {
            return "redirect:../test/start?nomor=" + nomor;
        }else{
            return "redirect:../test/start?nomor=1";
        }
    }


    @PostMapping("/test/answer_finish")
    public String testAnswerFinish(@RequestParam (required = true) SoalTest soalTest,
                             @RequestParam(required = false) String jawaban,
                             @RequestParam(required = false) String finish,
                             RedirectAttributes redirectAttributes,Authentication authentication){

        User user = currentUserService.currentUser(authentication);

        if(jawaban == null){
            redirectAttributes.addFlashAttribute("error", "Please Answer this question.!");
            return "redirect:../test/start?nomor=" + soalTest.getNomor();
        }

        if(jawaban.isEmpty()){
            redirectAttributes.addFlashAttribute("error", "Please Answer this question.!");
            return "redirect:../test/start?nomor=" + soalTest.getNomor();
        }

        SoalTest soalTest1 = soalTestDao.findById(soalTest.getId()).get();
        if(jawaban.equals("A")){
            soalTest1.setAnswera(Boolean.TRUE);
            soalTest1.setAnswerb(Boolean.FALSE);
            soalTest1.setAnswerc(Boolean.FALSE);
            soalTest1.setAnswerd(Boolean.FALSE);
            if(soalTest1.getSoal().getJawabanA() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }else if(jawaban.equals("B")){
            soalTest1.setAnswera(Boolean.FALSE);
            soalTest1.setAnswerb(Boolean.TRUE);
            soalTest1.setAnswerc(Boolean.FALSE);
            soalTest1.setAnswerd(Boolean.FALSE);
            if(soalTest1.getSoal().getJawabanB() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }else if(jawaban.equals("C")){
            soalTest1.setAnswera(Boolean.FALSE);
            soalTest1.setAnswerb(Boolean.FALSE);
            soalTest1.setAnswerc(Boolean.TRUE);
            soalTest1.setAnswerd(Boolean.FALSE);
            if(soalTest1.getSoal().getJawabanC() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }else if(jawaban.equals("D")){
            soalTest1.setAnswera(Boolean.FALSE);
            soalTest1.setAnswerb(Boolean.FALSE);
            soalTest1.setAnswerc(Boolean.FALSE);
            soalTest1.setAnswerd(Boolean.TRUE);
            if(soalTest1.getSoal().getJawabanD() == Boolean.TRUE){
                soalTest1.setStatusAnswer(StatusAnswer.CORRECT);
            }else{
                soalTest1.setStatusAnswer(StatusAnswer.INCORRECT);
            }
        }
        soalTest1.setJawaban(jawaban);
        soalTest1.setStatus(StatusRecord.DONE);
        soalTest1.setTanggalUpdate(LocalDateTime.now());
        soalTest1.setUserUpdate(user.getUsername());
        soalTestDao.save(soalTest1);

        return "redirect:../test/finish";
    }

    @GetMapping("/test/finish")
    public String testFinish(Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Context context = new Context();
        Subscribe subscribe = subscribeDao.findByEmail(user.getUsername());
        LocalDateTime timeExp = user.getWaktuSelesai();
        LocalDateTime resultTimeExp = timeExp.plus(6, ChronoUnit.MONTHS);
        context.setVariable("subscribe", subscribe);
        context.setVariable("exp", resultTimeExp);
        context.setVariable("user", user);

        User user1 = finishService.finishTest(user);
        try {
            pdfService.thmeleafGeneratePdfWithHtml("pdf/score", context, "Detail", user1.getId());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "test/form_finish";
    }


    @GetMapping("/test/finish_timer")
    public String testFinishTimer(Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Context context = new Context();
        Subscribe subscribe = subscribeDao.findByEmail(user.getUsername());
        context.setVariable("subscribe", subscribe);
        context.setVariable("user", user);

        User user1 = finishService.finishTest(user);
        try {
            pdfService.thmeleafGeneratePdfWithHtml("pdf/score", context, "Detail", user1.getId());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "test/form_finish";
    }


}
