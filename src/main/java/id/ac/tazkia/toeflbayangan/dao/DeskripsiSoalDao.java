package id.ac.tazkia.toeflbayangan.dao;

import id.ac.tazkia.toeflbayangan.entity.DeskripsiSoal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DeskripsiSoalDao extends PagingAndSortingRepository<DeskripsiSoal, String>, CrudRepository<DeskripsiSoal,String> {
}
