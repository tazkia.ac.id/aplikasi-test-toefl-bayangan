package id.ac.tazkia.toeflbayangan.dao;

import id.ac.tazkia.toeflbayangan.entity.KonversiNilai;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface KonversiNilaiDao extends PagingAndSortingRepository<KonversiNilai, Integer> {

    KonversiNilai findByJawaban(Integer jawaban);

}
