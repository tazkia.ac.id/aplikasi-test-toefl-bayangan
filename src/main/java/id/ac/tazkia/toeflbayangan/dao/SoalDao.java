package id.ac.tazkia.toeflbayangan.dao;

import id.ac.tazkia.toeflbayangan.entity.DeskripsiSoal;
import id.ac.tazkia.toeflbayangan.entity.Soal;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SoalDao extends PagingAndSortingRepository<Soal, String>, CrudRepository<Soal,String> {
    List<Soal> findByDeskripsiSoalOrderByNomor(DeskripsiSoal deskripsiSoal);
    List<Soal> findByDeskripsiSoalAndIdNotIn(DeskripsiSoal deskripsiSoal, List<String> id);
    Soal findFirstByDeskripsiSoal(DeskripsiSoal deskripsiSoal);

    Soal findByIdAndDeletedIsNull(String id);

    Soal findByIdAndDeletedIsNotNull(String id);


    @Query(value = "select id from soal where deleted is not null order by id_deskripsi_soal, nomor", nativeQuery = true)
    List<String> cariSoal();

    @Query("select aa from Soal as aa where aa.deleted is null order by aa.jenis asc, aa.deskripsiSoal.id asc , aa.nomor asc")
    List<Soal> cariSoalUjian();

    @Query(value = "SELECT id, id_deskripsi_soal, pertanyaan FROM soal where deleted is null", nativeQuery = true)
    List<Object[]> getAllSoals();

    @Query(value = "select COUNT(b.id) as jumlah_soal, b.id, b.deskripsi from soal as a left join deskripsi_soal as b on a.id_deskripsi_soal = b.id where a.deleted is null group by a.id_deskripsi_soal", nativeQuery = true)
    List<Object[]> getDeskripsi();

}
