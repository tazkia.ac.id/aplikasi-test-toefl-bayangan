package id.ac.tazkia.toeflbayangan.dao;

import id.ac.tazkia.toeflbayangan.constant.StatusAnswer;
import id.ac.tazkia.toeflbayangan.constant.StatusRecord;
import id.ac.tazkia.toeflbayangan.constant.StatusType;
import id.ac.tazkia.toeflbayangan.entity.Soal;
import id.ac.tazkia.toeflbayangan.entity.SoalTest;
import id.ac.tazkia.toeflbayangan.entity.config.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SoalTestDao extends PagingAndSortingRepository<SoalTest, String>, CrudRepository<SoalTest, String> {

    List<SoalTest> findByStatusAndUserOrderBySoalCreated(StatusRecord statusRecord, User user);

    SoalTest findByStatusNotAndUserAndSoal(StatusRecord statusRecord, User user, Soal soal);

    List<SoalTest> findByStatusNotAndUserOrderBySoalJenisAscSoalDeskripsiSoalCreatedAscSoalNomorAsc(StatusRecord statusRecord, User user);

    List<SoalTest> findByStatusNotAndUserOrderByNomor(StatusRecord statusRecord,User user);

//    Integer countByStatusNotAndUser(StatusRecord statusRecord, User user);

    SoalTest findByStatusNotAndUserAndNomor(StatusRecord statusRecord, User user, Integer nomor);

    Integer countByStatusNotAndUser(StatusRecord statusRecord, User user);

    Integer countByStatusNotAndStatusAnswerAndSoalJenisAndUser(StatusRecord statusRecord, StatusAnswer statusAnswer, StatusType statusType, User user);

}
