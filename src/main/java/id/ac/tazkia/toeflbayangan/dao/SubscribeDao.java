package id.ac.tazkia.toeflbayangan.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


import id.ac.tazkia.toeflbayangan.entity.Subscribe;

public interface SubscribeDao extends PagingAndSortingRepository<Subscribe, String>, CrudRepository<Subscribe, String> {
    Subscribe findByEmail(String email);
}
