package id.ac.tazkia.toeflbayangan.dao.config;

import id.ac.tazkia.toeflbayangan.entity.config.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String> {


}
