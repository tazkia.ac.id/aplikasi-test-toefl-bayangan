package id.ac.tazkia.toeflbayangan.dao.config;

import id.ac.tazkia.toeflbayangan.entity.config.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String>, CrudRepository<User, String> {
    User findByUsername(String username);
}
