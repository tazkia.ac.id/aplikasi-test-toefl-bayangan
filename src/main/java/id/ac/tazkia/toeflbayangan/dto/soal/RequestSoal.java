package id.ac.tazkia.toeflbayangan.dto.soal;

import id.ac.tazkia.toeflbayangan.constant.StatusType;
import lombok.Data;

@Data
public class RequestSoal {
    private String id;
    private String deskripsiId;
    private String deskripsi;
    private String pertanyaan;
    private String pilihanA;
    private String pilihanB;
    private String pilihanC;
    private String pilihanD;
    private String jawaban;
    private String jawabanA;
    private String jawabanB;
    private String jawabanC;
    private String jawabanD;
    private StatusType jenis;
    private Integer nomor;
}
