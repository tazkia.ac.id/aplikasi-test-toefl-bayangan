package id.ac.tazkia.toeflbayangan.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;

@Data
@Entity
public class DeskripsiSoal extends BaseEntity {
    @Column(columnDefinition = "LONGTEXT")
    private String deskripsi;
}
