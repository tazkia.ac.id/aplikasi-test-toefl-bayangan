package id.ac.tazkia.toeflbayangan.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class KonversiNilai {

    @Id
    private Integer jawaban;

    private Integer listening;

    private Integer structure;

    private Integer reading;


}
