package id.ac.tazkia.toeflbayangan.entity;

import id.ac.tazkia.toeflbayangan.constant.StatusType;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Data
@Entity
@SQLDelete(sql = "UPDATE soal SET deleted=now() WHERE id=?")
@Where(clause = "deleted IS NULL")
public class Soal extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "id_deskripsi_soal")
    private DeskripsiSoal deskripsiSoal;

    @Column(columnDefinition = "LONGTEXT")
    @NotNull
    private String pertanyaan;

    @Column(columnDefinition = "LONGTEXT", name = "pilihan_a")
    @NotNull
    private String pilihanA;

    @Column(columnDefinition = "LONGTEXT", name = "pilihan_b")
    @NotNull
    private String pilihanB;

    @Column(columnDefinition = "LONGTEXT", name = "pilihan_c")
    @NotNull
    private String pilihanC;

    @Column(columnDefinition = "LONGTEXT", name = "pilihan_d")
    @NotNull
    private String pilihanD;

    @Column(name = "jawaban_a")
    private Boolean jawabanA = false;

    @Column(name = "jawaban_b")
    private Boolean jawabanB = false;

    @Column(name = "jawaban_c")
    private Boolean jawabanC = false;

    @Column(name = "jawaban_d")
    private Boolean jawabanD = false;

    @Enumerated(EnumType.STRING)
    private StatusType jenis;

    private Integer nomor;
}
