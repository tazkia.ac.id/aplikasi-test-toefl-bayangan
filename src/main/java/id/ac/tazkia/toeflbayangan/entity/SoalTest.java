package id.ac.tazkia.toeflbayangan.entity;

import id.ac.tazkia.toeflbayangan.constant.StatusAnswer;
import id.ac.tazkia.toeflbayangan.constant.StatusRecord;
import id.ac.tazkia.toeflbayangan.constant.StatusType;
import id.ac.tazkia.toeflbayangan.entity.config.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;

@Entity
@Data
public class SoalTest {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_soal")
    private Soal soal;

    private Boolean answera;
    private Boolean answerb;
    private Boolean answerc;
    private Boolean answerd;

    private Integer nomor;

    private String userUpdate;

    private LocalDateTime tanggalUpdate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

    @Enumerated(EnumType.STRING)
    private StatusAnswer statusAnswer;

    private String jawaban;

}
