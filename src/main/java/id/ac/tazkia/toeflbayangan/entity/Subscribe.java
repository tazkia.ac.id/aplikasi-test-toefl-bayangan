package id.ac.tazkia.toeflbayangan.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import id.ac.tazkia.toeflbayangan.constant.RecordStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Subscribe {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    private String nama;

    private String noWa;
    
    private String email;

    private String asalKota;

    private String asalSekolah;

//    @Enumerated(EnumType.STRING)
//    private RecordStatus status = RecordStatus.ACTIVE;

    private String status;

    private LocalDateTime tglInsert;

    private String userFu;

    private String userUpdate;

    private LocalDateTime tglUpdate;

    private Integer hubungi;

    private String sumber;

    private String kelas;

    private String idTahunajaran;

    private String tahunLulus;

    private String kelasSma;

    private String jenjang;

    private String noteEc;

    private String lulusan;
}
