package id.ac.tazkia.toeflbayangan.entity.config;


import lombok.Data;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "table_permission")
@Data
public class Permission {

    @Id
    private String id;
    @Column(name = "permission_label")
    private String label;
    @Column(name = "permission_value")
    private String value;

}
