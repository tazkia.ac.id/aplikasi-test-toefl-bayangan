package id.ac.tazkia.toeflbayangan.entity.config;


import id.ac.tazkia.toeflbayangan.constant.RecordStatus;
import id.ac.tazkia.toeflbayangan.constant.StatusRecord;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "table_user")
@Data
public class User {

    @Id
    private String id;
    private String username;

    @Enumerated(EnumType.STRING)
    private RecordStatus active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

    private LocalDateTime waktuMulai;

    private LocalDateTime waktuSelesai;

    private Integer reading;
    private Integer konversiReading;
    private Integer structure;
    private Integer konversiStructure;
    private Integer total;
    private Integer nilaiTotal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

}
