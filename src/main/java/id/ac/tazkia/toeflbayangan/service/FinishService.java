package id.ac.tazkia.toeflbayangan.service;


import id.ac.tazkia.toeflbayangan.constant.StatusAnswer;
import id.ac.tazkia.toeflbayangan.constant.StatusRecord;
import id.ac.tazkia.toeflbayangan.constant.StatusType;
import id.ac.tazkia.toeflbayangan.dao.KonversiNilaiDao;
import id.ac.tazkia.toeflbayangan.dao.SoalTestDao;
import id.ac.tazkia.toeflbayangan.dao.config.UserDao;
import id.ac.tazkia.toeflbayangan.entity.KonversiNilai;
import id.ac.tazkia.toeflbayangan.entity.config.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FinishService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SoalTestDao soalTestDao;

    @Autowired
    private KonversiNilaiDao konversiNilaiDao;

    public User finishTest(User user){

        Integer jumlahStructure = soalTestDao.countByStatusNotAndStatusAnswerAndSoalJenisAndUser(StatusRecord.DELETED, StatusAnswer.CORRECT, StatusType.STRUCTURE, user);
        Integer jumlahReading = soalTestDao.countByStatusNotAndStatusAnswerAndSoalJenisAndUser(StatusRecord.DELETED, StatusAnswer.CORRECT, StatusType.READING, user);

        jumlahStructure = jumlahStructure * 2;
        jumlahReading = jumlahReading * 2;

        KonversiNilai konversiNilaiStructure = konversiNilaiDao.findByJawaban(jumlahStructure);
        KonversiNilai konversiNilaiReading = konversiNilaiDao.findByJawaban(jumlahReading);

        user.setReading(jumlahReading);
        user.setKonversiReading(konversiNilaiReading.getReading());
        user.setStructure(jumlahStructure);
        user.setKonversiStructure(konversiNilaiStructure.getStructure());
        user.setTotal(konversiNilaiReading.getReading()+konversiNilaiStructure.getStructure());
        user.setNilaiTotal((user.getTotal()/2)*10);
        user.setStatus(StatusRecord.FINISH);
        userDao.save(user);

        return user;
    }

}
