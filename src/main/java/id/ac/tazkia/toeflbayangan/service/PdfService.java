package id.ac.tazkia.toeflbayangan.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.openhtmltopdf.pdfboxout.PdfBoxRenderer;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PdfService {

    @Value("${app.url}")
    public String baseUrl;

    @Value("${app.folder.generated}")
    public String generatedDir;

    private final DateTimeFormatter formatDataFile = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    public File thmeleafGeneratePdfWithHtml(String file, Context context, String type, String filename) throws Exception{
        String fileLocation = generatedDir + File.separator + type + File.separator + filename + ".pdf";

        File outputFile = new File(fileLocation);

        if (!outputFile.exists()) {
            outputFile.getParentFile().mkdirs();
        }
        
        try (OutputStream os = new FileOutputStream(outputFile)) {

            String html = thymeleafTransformedContent(file, context);
            String baseUri = new ClassPathResource("templates/"+file+".html").getURL().toExternalForm();

            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();
            builder.withHtmlContent(html,baseUri);
            builder.toStream(os);
            builder.useDefaultPageSize(210, 297, PdfRendererBuilder.PageSizeUnits.MM);
            builder.run();
            return outputFile;
        } catch (Exception e) {
            log.error("Error CREATING PDF ", e);
            throw new Exception("Gagal membuat pdf",e);

        }
    }

    private String thymeleafTransformedContent(String fileName, Context context) {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        return templateEngine.process(fileName, context);
    }

    
}
