package id.ac.tazkia.toeflbayangan.service;

import id.ac.tazkia.toeflbayangan.dao.DeskripsiSoalDao;
import id.ac.tazkia.toeflbayangan.dao.SoalDao;
import id.ac.tazkia.toeflbayangan.dto.soal.RequestSoal;
import id.ac.tazkia.toeflbayangan.entity.DeskripsiSoal;
import id.ac.tazkia.toeflbayangan.entity.Soal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class SoalService {
    @Autowired
    private SoalDao soalDao;
    @Autowired
    private DeskripsiSoalDao deskripsiSoalDao;

    public Soal saveSoal(RequestSoal requestSoal) {
        if (requestSoal.getId().isEmpty()) {
            log.info("Soal Id not Exist");
            Soal soal = new Soal();
            soal.setPertanyaan(requestSoal.getPertanyaan());
            soal.setJenis(requestSoal.getJenis());
            soal.setPilihanA(requestSoal.getPilihanA());
            soal.setPilihanB(requestSoal.getPilihanB());
            soal.setPilihanC(requestSoal.getPilihanC());
            soal.setPilihanD(requestSoal.getPilihanD());
            soal.setNomor(requestSoal.getNomor());
            switch (requestSoal.getJawaban()){
                case "A" -> {
                    soal.setJawabanA(true);
                }

                case "B" -> {
                    soal.setJawabanB(true);
                }

                case "C" -> {
                    soal.setJawabanC(true);
                }

                default -> {
                    soal.setJawabanD(true);
                }

            }
            if (requestSoal.getDeskripsiId() != null){
                DeskripsiSoal deskripsiSoal = deskripsiSoalDao.findById(requestSoal.getDeskripsiId()).get();
                deskripsiSoal.setDeskripsi(requestSoal.getDeskripsi());
                deskripsiSoalDao.save(deskripsiSoal);
                soal.setDeskripsiSoal(deskripsiSoal);
            }

            soalDao.save(soal);

            if (!requestSoal.getDeskripsi().isEmpty() && requestSoal.getDeskripsiId() == null) {
                createDescription(soal, requestSoal.getDeskripsi());
            }

            return soal;
        } else {
            log.info("Soal Id is Exist");
            Optional<Soal> optionalSoal = soalDao.findById(requestSoal.getId());

            if (optionalSoal.isPresent()) {
                Soal soal = optionalSoal.get();
                soal.setPertanyaan(requestSoal.getPertanyaan());
                soal.setJenis(requestSoal.getJenis());
                soal.setPilihanA(requestSoal.getPilihanA());
                soal.setPilihanB(requestSoal.getPilihanB());
                soal.setPilihanC(requestSoal.getPilihanC());
                soal.setPilihanD(requestSoal.getPilihanD());
                soal.setNomor(requestSoal.getNomor());
                switch (requestSoal.getJawaban()){
                    case "A" -> {
                        soal.setJawabanA(true);
                    }

                    case "B" -> {
                        soal.setJawabanB(true);
                    }

                    case "C" -> {
                        soal.setJawabanC(true);
                    }

                    default -> {
                        soal.setJawabanD(true);
                    }

                }

                if (requestSoal.getDeskripsiId() != null){
                    DeskripsiSoal deskripsiSoal = deskripsiSoalDao.findById(requestSoal.getDeskripsiId()).get();
                    deskripsiSoal.setDeskripsi(requestSoal.getDeskripsi());
                    deskripsiSoalDao.save(deskripsiSoal);
                    soal.setDeskripsiSoal(deskripsiSoal);
                }

                soalDao.save(soal);
                if (!requestSoal.getDeskripsi().isEmpty() && requestSoal.getDeskripsiId() == null) {
                    createDescription(soal, requestSoal.getDeskripsi());
                }
            }
            return optionalSoal.get();
        }

    }

    public DeskripsiSoal createDescription(Soal soal, String deskripsi) {
        DeskripsiSoal deskripsiSoal;
        if (soal.getDeskripsiSoal() == null) {
            deskripsiSoal = new DeskripsiSoal();
            deskripsiSoal.setDeskripsi(deskripsi);
            deskripsiSoalDao.save(deskripsiSoal);
            soal.setDeskripsiSoal(deskripsiSoal);
            soalDao.save(soal);
        } else {
            deskripsiSoal = soal.getDeskripsiSoal();
            deskripsiSoal.setDeskripsi(deskripsi);
            deskripsiSoalDao.save(deskripsiSoal);
            soal.setDeskripsiSoal(deskripsiSoal);
            soalDao.save(soal);
        }
        return deskripsiSoal;

    }
}
