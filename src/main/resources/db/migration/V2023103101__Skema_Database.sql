create table deskripsi_soal
(
    id         varchar(50) not null,
    created    timestamp(6),
    created_by varchar(100),
    updated    timestamp(6),
    updated_by varchar(100),
    deleted    timestamp(6),
    deskripsi  longtext,
    primary key (id)
);

create table soal
(
    id                varchar(36) not null,
    created           timestamp(6),
    created_by        varchar(100),
    updated           timestamp(6),
    updated_by        varchar(100),
    deleted           timestamp(6),
    id_deskripsi_soal varchar(50),
    jenis             varchar(50),
    pertanyaan        longtext    not null,
    pilihan_a         longtext    not null,
    pilihan_b         longtext    not null,
    pilihan_c         longtext    not null,
    pilihan_d         longtext    not null,
    jawaban_a         boolean,
    jawaban_b         boolean,
    jawaban_c         boolean,
    jawaban_d         boolean,
    primary key (id)
);