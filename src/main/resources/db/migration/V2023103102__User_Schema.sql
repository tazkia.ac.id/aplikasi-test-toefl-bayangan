-- CREATE TABLE `subscribe` (
--                              `id` varchar(45) COLLATE utf8mb4_general_ci NOT NULL,
--                              `nama` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `no_wa` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `email` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `asal_kota` varchar(125) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `asal_sekolah` varchar(125) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `status` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `tgl_insert` datetime DEFAULT NULL,
--                              `user_fu` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `user_update` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `tgl_update` datetime DEFAULT NULL,
--                              `hubungi` int DEFAULT NULL,
--                              `sumber` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `kelas` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `id_tahunajaran` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `tahun_lulus` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `kelas_sma` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `jenjang` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `note_ec` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              `lulusan` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
--                              PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `table_permission` (
                                    `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                    `permission_label` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                    `permission_value` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `table_role_permission` (
                                         `id_role` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                                         `id_permission` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `table_role` (
                              `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                              `description` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                              `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

CREATE TABLE `table_user` (
                              `id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                              `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                              `active` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                              `id_role` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
                              `user` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
