CREATE TABLE `soal_test` (
                             `id` varchar(45) NOT NULL,
                             `id_user` varchar(45) DEFAULT NULL,
                             `id_soal` varchar(45) DEFAULT NULL,
                             `answera` tinyint(1) DEFAULT NULL,
                             `answerb` tinyint(1) DEFAULT NULL,
                             `answerc` tinyint(1) DEFAULT NULL,
                             `answerd` tinyint(1) DEFAULT NULL,
                             `status` varchar(45) DEFAULT NULL,
                             `nomor` int DEFAULT NULL,
                             `user_update` varchar(45) DEFAULT NULL,
                             `tanggal_update` datetime DEFAULT NULL,
                             `status_answer` varchar(45) DEFAULT NULL,
                             PRIMARY KEY (`id`)
);
