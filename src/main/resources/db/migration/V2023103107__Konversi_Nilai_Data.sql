INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (50,68,0,67);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (49,67,0,66);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (48,66,0,65);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (47,65,0,63);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (46,63,0,61);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (45,62,0,60);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (44,61,0,59);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (43,60,0,58);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (42,59,0,57);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (41,58,0,56);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (40,57,68,55);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (39,57,67,54);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (38,56,65,54);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (37,55,63,53);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (36,54,61,52);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (35,54,60,52);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (34,53,58,51);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (33,52,57,50);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (32,52,56,49);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (31,51,55,48);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (30,51,54,48);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (29,50,53,47);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (28,49,52,46);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (27,49,51,46);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (26,48,50,45);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (25,48,49,44);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (24,47,48,43);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (23,47,47,43);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (22,46,46,42);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (21,45,45,41);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (20,45,44,40);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (19,44,43,39);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (18,43,43,38);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (17,42,41,37);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (16,41,40,36);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (15,41,40,35);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (14,39,38,34);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (13,38,37,32);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (12,37,36,31);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (11,35,35,30);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (10,33,33,29);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (9,32,31,28);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (8,32,29,28);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (7,31,27,27);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (6,30,26,26);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (5,29,25,25);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (4,28,23,24);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (3,27,22,23);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (2,26,21,23);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (1,25,20,22);
INSERT INTO `konversi_nilai` (`jawaban`,`listening`,`structure`,`reading`) VALUES (0,24,20,21);

INSERT INTO `table_permission` (`id`,`permission_label`,`permission_value`) VALUES ('ujian','Ujian','UJIAN');

INSERT INTO `table_role_permission` (`id_role`,`id_permission`) VALUES ('peserta','ujian');

INSERT INTO `table_role` (`id`,`description`,`name`) VALUES ('peserta','Peserta','PESERTA');

INSERT INTO `table_user` (`id`,`username`,`active`,`id_role`,`user`,`waktu_mulai`,`waktu_selesai`,`reading`,`konversi_reading`,`structure`,`konversi_structure`,`total`,`nilai_total`,`status`) VALUES ('001','suprayogi20@gmail.com','ACTIVE','peserta','-','2023-11-14 08:07:23','2023-11-14 09:07:23',1,22,0,20,42,210,'FINISH');

INSERT INTO `deskripsi_soal` (`id`,`created`,`created_by`,`updated`,`updated_by`,`deleted`,`deskripsi`) VALUES ('a75d3f83-e0a6-46f0-ad98-27f915338e53','2023-11-09 01:36:30.282070',NULL,NULL,NULL,NULL,'<p>Deskripsi Satu</p>');

INSERT INTO `soal` (`id`,`created`,`created_by`,`updated`,`updated_by`,`deleted`,`id_deskripsi_soal`,`jenis`,`pertanyaan`,`pilihan_a`,`pilihan_b`,`pilihan_c`,`pilihan_d`,`jawaban_a`,`jawaban_b`,`jawaban_c`,`jawaban_d`) VALUES ('5d043630-f7be-47dd-a672-70ad59e503ed','2023-11-09 01:37:01.885722',NULL,NULL,NULL,NULL,'a75d3f83-e0a6-46f0-ad98-27f915338e53','STRUCTURE','<p>Peranyaan Dua</p>','<p>Jawaban A</p>','<p>Jawaban B</p>','<p>Jawaban C</p>','<p>Jawaban D</p>',0,0,0,1);
INSERT INTO `soal` (`id`,`created`,`created_by`,`updated`,`updated_by`,`deleted`,`id_deskripsi_soal`,`jenis`,`pertanyaan`,`pilihan_a`,`pilihan_b`,`pilihan_c`,`pilihan_d`,`jawaban_a`,`jawaban_b`,`jawaban_c`,`jawaban_d`) VALUES ('9054e73d-587f-4bb8-a60b-76715f8146d5','2023-11-09 01:36:30.184498',NULL,NULL,NULL,NULL,'a75d3f83-e0a6-46f0-ad98-27f915338e53','READING','<p>Pertanyaan Satu</p>','<p>Jawaban a</p>','<p>Jawaban B</p>','<p>Jawaban C</p>','<p>Jawaban D</p>',1,0,0,0);

INSERT INTO `soal_test` (`id`,`id_user`,`id_soal`,`answera`,`answerb`,`answerc`,`answerd`,`status`,`nomor`,`user_update`,`tanggal_update`,`status_answer`) VALUES ('00b2d0f4-3805-4d1c-8baa-b73b97c0c2b2','001','5d043630-f7be-47dd-a672-70ad59e503ed',0,0,1,0,'DONE',1,'suprayogi20@gmail.com','2023-11-15 06:23:09','INCORRECT');
INSERT INTO `soal_test` (`id`,`id_user`,`id_soal`,`answera`,`answerb`,`answerc`,`answerd`,`status`,`nomor`,`user_update`,`tanggal_update`,`status_answer`) VALUES ('47f2ce18-4114-4fbe-92a7-0aec6442bd2d','001','9054e73d-587f-4bb8-a60b-76715f8146d5',1,0,0,0,'DONE',2,'suprayogi20@gmail.com','2023-11-15 06:23:12','CORRECT');


